from django.urls import path
from . import views

urlpatterns = [
    path('main.css', views.style),
    path('', views.index, name='index'),
    path('<name>', views.page, name='page'),
]

